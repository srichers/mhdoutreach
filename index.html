<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!-- The template comes from the CU Boulder template bank at http://assett.colorado.edu/tools/templates -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>3D Magnetorotational Core-Collapse</title>
<link href="defaultstyle.css" rel="stylesheet" media="screen" />
</head>
<body>
<div id="container">
<div id="wrapper">
    <div id="banner">
    	<div id="name">
    	<h1>3D Magnetorotational Core-Collapse</h1>
    	<p>Philipp Moesta</p>
	<p>Sherwood Richers</p>
    	</div>
    </div>
<div id="page">

    <div id="content">
<p>In a second, the <b>entire human race</b> uses around 3*10<sup>22</sup> ergs of energy. In the same second, <b>the sun</b> radiates a gargantuant 4*10<sup>33</sup> ergs, over 10<sup>11</sup> times as much. However, both of these are dwarfed by a standard <b>core-collapse supernova explosion</b> emitting on the order of 10<sup>53</sup> ergs in the very same second. These incredible energies cause the formation of the heavy elements that make up <b>asteroids, planets, and life</b>. Collapsing stars are among the most energetic and luminous events in our universe, and though we have learned much since the first recorded supernova in 185 A.D., the precise mechanism that causes stars to explode is rather poorly understood. Furthermore, 1-2%% of supernovae are 10x more energetic than a canonical supernova, and may be related to the mysterious <b>gamma-ray bursts</b>. Recently, we simulated the early stages of a model star thought to lead to such an extra-energetic "hypernova" and found that it behaves very differently than was previously thought.</p>

If you would like to know more than is on this page, there is a variety of places this work has appeared.
<ul>
  <li>Our paper in <a href="http://adsabs.harvard.edu/abs/2014ApJ...785L..29M">The Astrophysical Journal Letters</a> (authoritative)</li>
  <li>Code details on <a href="http://stellarcollapse.org/cc3dgrmhd">stellarcollapse.org</a></li>
  <li>An article in <a href="">The Huffington Post</a></li>
  <li>Christian Ott's <a href="http://blowingupstars.blogspot.com/2014/04/nature-has-it-in-for-us-supernovae-from.html">Blog</a></li>
</ul>

<br/><hr><br/>
<center><h1>Introduction to Core-Collapse Supernovae</h1></center>
<p>The story of a dying star has something for everybody. It is rich with a diversity of physics that few others can match, is an important piece of our own origin story, and is naturally beautiful. There are many ways to end a star's life, determined mostly by the mass of the star, but we will stick with the supernova explosions caused by the collapse of stars with masses from about 8 to upwards of 100 times the mass of our sun.</p>

  <br/>
  <center><h2> The Canonical Supernova</h2></center>
  <p>Though the devil is in the details, it is known that massive stars will go through a few basic steps that lead to explosion.</p>

  <div id="adjacentbarL">
  <h3>1) A massive star builds an iron core</h3>
  <p>The typical story starts with a star 8-10 times the mass of our sun, initially made almost entirely of hydrogen. As it ages, hydrogen is fused into heavier and heavier elements in the star's core. The star's huge mass pressurizes the core, which makes the core fuse atoms at a phenomenal rate. In all stars, there is a constant struggle between <b>gravity</b> trying to pull the mass inward, and the <b>pressure</b> from thermal motion and <a href="http://en.wikipedia.org/wiki/Degenerate_matter#Electron_degeneracy">quantum degeneracy</a> (i.e. due to the Pauli Exclusion Principle) pushing everything outward. After a mere few millions to tens of millions of years, the center of the star is made of very dense iron. Fusing iron into heavier elements does not release energy like fusing lighter elements did, so the star can no longer support the mass resting on it and collapses under its own gravity.</p>
  </div>

  <div id="contentbarR" class="contentbar1">
    <center><p><a href="images/cc_picture.png"><img src="images/cc_picture.png" width="200"/></a><br/>
      The star (top) has an iron core (zoomed in, middle) that collapses and sends a shock outward (bottom).
    </p></center>
  </div>

  <br style="clear:right"></br>
  <div id="contentbarL" class="contentbar1">
    <table style="width:300px" border="1">
      <tr><th>Matter</th><th>Density (g/cm<sup>3</sup>)</th></tr>
      <tr><td align="center">Water</td><td align="center">1</td></tr>
      <tr><td align="center">Lead</td><td align="center">11.34</td></tr>
      <tr><td align="center">Center of the Sun</td><td align="center">150</td></tr>
      <tr><td align="center">White Dwarf</td><td align="center">10<sup>6</sup></td></tr>
      <tr><td align="center">Atomic Nucleus</td><td align="center">2*10<sup>14</sup></td></tr>
      <tr><td align="center">Core at Bounce</td><td align="center">6*10<sup>14</sup></td></tr>
    </table>
	
  </div>	        

  <div id="adjacentbarR">
  <h3>2) The iron core collapses and a shockwave moves outward </h3>
   <p>In most cases, the star will not collapse directly to a black hole. Once the core reaches a density of around 10<sup>14</sup> g/ccm the <b>strong nuclear force</b> suddently halts the collapse and causes the core to partially <b>bounce</b> back outward faster than sound waves can travel. This creates an outward-moving <b>shock wave</b> that was once thought to be enough to explode the rest of the star. But the energy required to move a shock front through many solar masses of infalling star and to shatter all the atoms back into free neutrons and protons takes a hefty toll on the shock.
   </div>
  
  <br style="clear:left"></br>
  <div id="adjacentbarL">
    <h3> 3) The shock stalls, but is revived by neutrinos </h3>
    <p>Though it is trying to escape, the rest of the star is simply falling inward too fast. The shock <b>stalls</b>, as the matter outside the shock wave falls inward as fast as the shock can propagate outward through it. At this point, it is still largely unknown what <b>revives</b> the shock and causes it to blow through the rest of the star. The solution to this problem is the ultimate goal of supernova theory. The <b>neutrino mechanism</b> has become the canonical hypothesis, by which neutrinos emitted from the core heat the matter under the shock, providing the extra energy required to push the shock outward.</p>
  </div>

  <div id="contentbarR" class="contentbar1">
    <center><p><a href="images/postbounce_config.png"><img src="images/postbounce_config.png" width="200"/></a><br/>
      Neutrinos heat the volume under the shock, giving it the energy it needs to explode.</center>
    </p>
  </div>	        
  
  <br style="clear:right"></br>
  <div id="contentbarL" class="contentbar1"><br/><br/><br/><br/>
    <center><p><a href="images/CassA.jpg"><img src="images/CassA.jpg" width="200"/></a><br/>
      Cassiopeia A, the remnants of a star that exploded in our galaxy around 300 years ago.</center>
    </p>
  </div>	        

  <div id="adjacentbarR">
  <h3> 4) The shock breaks through the star's surface </h3>  
  <p>However nature revives the shock, we do observe the explosions. The shock takes typically <b>a few hours</b> to move through the rest of the star, and finally explodes through the star's surface. The debris left behind recombines into radioactive elements that slowly decay, emitting light for <b>days to weeks</b>. Following this, the remnants continue to glow for a few more <b>months</b>. The high-density core is left in the middle, and proceeds to cool and become a <b>neutron star</b>.</p>
  
  <p>Unfortunately, it is very difficult to catch a supernova right as it is exploding. Most observations of supernova remnants are made hours after the explosion, and the first observation of the beginnings of a supernova was only in 2008 (SN 2008D). In addition, the most mysterious part of the process is deep in the star, far beyond what we could see even if we were lucky enough to watch the star explode (though <b>neutrinos</b> and <b>gravitational waves</b> can give us some information - stay tuned for news from neutrino detectors and LIGO!). We must try to fill the gap between the observed stars and the observed remnants, and we do this with computer simulations.</p>
  </div>
  
  <br style="clear:right"></br><br/>
  <center><h2>Hypernovae are Exceptional</h2></center>
  <div id="contentbarR" class="contentbar1">
    <center><p><a href="images/W49.png"><img src="images/W49.png" width="200"/></a><br/>
      W49B, a supernova that exploded about 1000 years ago, may have been a Ic-bl supernova.</center>
  </p>
</div>	        
<div id="adjacentbarL">
  <p>Many supernovae are very odd and look very different from standard supernovae as described above. For instance, one class of supernovae, called <b>type Ic-bl</b> (bl for "broad line"), have spectral lines that are <a href="http://en.wikipedia.org/wiki/Doppler_broadening">doppler-broadened</a> by the large outflow velocities, and explode with around ten times more energy than standard core-collapse supernovae. About 1% of type Ic-bl CCSNe have also been observed at the same time and place as a <b>long gamma-ray burst</b> or <b>X-ray flash</b>. While the neutrino mechanism is a plausible means of blowing up the vast majority of massive stars, it cannot yield the energies needed to explain these supernovae. An entirely different mechanism is required</p>
</div>

  <br style="clear:left"></br>
  <h3> The Story, Revisited </h3>
  <p>It is likely these explosions are caused by much more massive stars. Spectra of type Ic supernovae suggest that the progenitor star was stripped of its hydrogen layer. <b>Wolf-Rayet</b> stars are a class of star that are so bright they expel their own hydrogen layers via winds, or are stars that are stripped of their hydrogen by a companion star (though we see Wolf-Rayet stars, how they form and what properties they have is still under debate). By the time an initially 25-solar-mass Wolf-Rayet star is about to collapse, all that is left is the dense inner core of about 6 solar masses, made of the various products of the nuclear fusion (He, C, Ne, O, Si and Fe).</p>
  <center><a href="images/engine_picture.png"><img src="images/engine_picture.png" width="800"/></a></center>
<br/>
<div id="adjacentbarL">
<p>A small fraction of massive stars may evolve to rapidly rotating Wolf-Rayet stars that have sufficient rotational energy to power a hypernova, if only there is a way to tap that energy. In rapidly spinning systems, such an explosion may be triggered by the <b>magneto-rotational mechanism</b>, in which a star with a pre-collapse magnetic field of around 10<sup>9</sup> G collapses and causes very strong magnetic fields (B > 10<sup>15</sup> G) to be built up inside the shock front by <b>flux compression</b>, <b>winding</b>, and/or the <b>magnetorotational instability</b> (MRI) (see below). For comparison, a typical fridge magnet has a magnetic field strength of around 50 G. The strong magnetic fields exert a large magnetic pressure, triggering a very energetic explosion. Many previous 2D simulations (simulations that simulate only a slice through the star rather than the whole star) show that this system produces a large bipolar jet-driven explosion that leaves behind a highly spinning and strongly magnetized proto-neutron star, also called a proto-magnetar.</p>
</div>
<div id="contentbarR">
    <center><p><a href="images/burrows.jpg"><img src="images/burrows.jpg" width="200"/></a><br/>
      A 2D simulation showing tightly-wound magnetic fields and a jet-driven explosion.</center>  
</div>

<table width="100%" cellpadding="0" cellspacing="20" border="0">
  <tr>
    <th width="33%" valign="top">Flux Compression</th>
    <th width="33%" valign="top">Magnetic Winding</th>
    <th width="33%" valign="top">Magnetorotational Instability (MRI)</th>
  </tr>
  <tr>
    <td>
      When the core collapses, magnetic field lines are squeezed closer together, and a higher density of field lines corresponds to a stronger field. This is expected to amplify the field by a factor of about 1000.
    </td>
    <td width="33%" valign="top">
      Collapse also causes the star to have strong differential rotation (i.e. inner parts rotating much faster than outer parts). Magnetic field lines get wound up, creating a strong field wound around the spin axis.
    </td>
    <td width="33%" valign="top">
      In differentially rotating plasmas (i.e. plasmas where inner regions rotate faster than outer regions), this instability will very rapidly amplify the magnetic field strength and will create strong turbulence. The effect this has on supernovae is currently poorly understood.
    </td>
  </tr>
</table>


  <br style="clear:right"></br>
  <hr><br/>
  <center><h1>Simulating Extreme Supernovae on a Computer</h1></center>
  <center>
  <a href="images/stampede.jpg"><img src="images/stampede.jpg" width="200"/></a>
  <a href="images/kraken.jpg"><img src="images/kraken.jpg" width="200"/></a>
  <a href="images/bluewaters.jpg"><img src="images/bluewaters.jpg" width="200"/></a>
  </center>
  <br/>
  <p>In our simulations, we employ (take a breath) <b>3D GRMHD with neutrino leakage and a hot nuclear equation of state</b> (see below for a translation), and run on about 15,000 cores for a few months. This means it was an extremely sophisticated calculation, and in fact was the first such simulation with its level of sophistication. Let's break down what all this means.</p>
  <ul>
<li><b>3D</b> - "Three Dimensional" - Our simulations are three-dimensional. Many simulations are done in 1D (simulating only along a line through the star) or 2D (simulating only on a slice through the star), meaning they cannot capture things that move around in all three dimensions. Our simulation makes no such assumptions.</li>
<li><b>GR</b> - "General Relativistic" - Einstein's general theory of relativity is completely accounted for in these simulations. This means that the code is capable of simulating the formation of black holes, but GR has significant effects on supernovae even if no black hole is formed.</li>
<li><b>MHD</b> - "Magnetohydrodynamic" - We simulate an ideal fluid (i.e. no viscosity) and magnetic fields with no resistivity. This is a very good approximation in supernova conditions.</li>
<li><b>Neutrino Leakage</b> - We have an approximate means of determining the rate at which neutrinos are emitted from and absorbed by matter at different locations. In the future this will need to be upgraded to a more sophisticated scheme, but this is approximately accurate.</li>
<li><b>Hot Nuclear Equation of State</b> - Given a temperature, density, and electron fraction (the number of electrons divided by the number density of neutrons+protons in a given volume), the equation of state gives the gas pressure. Many versions exist based on different assumptions, since the nuclear forces are not yet well understood, and the choice of equation of state significantly affects the outcome.</li>
</ul>

  <br>
  <h3>Simulations vs Reality</h3>
  <p>Everything done on a computer is only an approximation of nature, and it is important to know that no simulation is perfect. The limitations of a simulation are as important as the results, since we need to know how closely our simulations represent reality.</p>
  <p>Our resolution is not fine enough to resolve an effect called the "magnetorotational instability" (MRI) that converts rotational energy into strong magnetic fields. This is only because doing the simulation at the necessary resolution is currently computationally impossible. In order to account for this field amplification that we can't resolve, the magnetic field we start with is actually about 1000 times stronger than the largest estimates from stellar evolution calculations. Our assumption is that this will have a similar effect as the strong fields created by the MRI, but this, of course, must be verified in the future.</p>

  <br/><hr><br/>
  <center><h1>We Found Something New</h1></center>
  <p>We wanted to know whether magnetic fields that get wound up and amplified by the star's rotation are able to transfer enough energy from rotation to thermal energy in order to <b>1)</b> produce the observed hyper-energetic explosions and <b>2)</b> produce the asymmetries observed in actual supernova remnants. To do this, we set up an experiment. We started with a Wolf-Rayet star that was evolved up to the collapse stage by ASDF. We made it briskly rotate, gave it a strong magnetic field (10<sup>12</sup> G), and let it collapse. We followed the collapse until 185 milliseconds after the core bounced, at which point we had to stop the simulation because it became too computationally expensive. You can see the brand new results for yourself below!</p>

  <br/>
  <h3><u>New Result</u>: The Jet is Unstable in 3D</h3>
  <p>Previously, others have done similar simulations in two dimensions, meaning they simulate a slice through the star rather than the full star. These simulations produce a profound, clean jet in both directions along the star's spin axis (like in the right side of the movie below). However, it turns out that this jet is very unstable, and is only artificially stabilized by running the simulation in 2D rather than 3D. The movie below shows a slice of the supernova with the spin axis up/down, color coded by entropy (so the redder regions are in some sense hotter than the blue regions).</p>
<center>
<iframe width="560" height="315" src="//www.youtube.com/embed/hJAY22ZjBMY" frameborder="0" allowfullscreen></iframe>
  <div id="contentbarR" class="contentbar1">
    <center><p><a href="images/kink,gif"><img src="images/kink.gif" width="200"/></a><br/>
      Types of perturbations. m=0 is a "sausage" mode, m=1 is a "kink" mode, other modes are "fluting" modes.</center>
  </p>
  </div>	        
</center>
<p>"Unstable" means that if you ever so slightly perturb something, this perturbation will grow, and will grow faster as the perturbation gets bigger. But there are many ways to perturb the young proto-jet. One could pinch the jet in a sausage-like pattern along the jet. Alternatively, one can make a perturbation in a helical pattern along the jet. Or, squeeze it in one direction and stretch it in the other. Only the sausage-like (m=0) perturbations can exist in 2D simulations, but the helix-like ("kink", m=1) perturbations are by far the fastest growing. So, by running our simulation in 3 dimensions, we don't artificially forbid the perturbations that completely disrupt the jet.</p>

  <br style="clear:right"></br>
  <h3><u>New Result</u>: Flux Tubes Fill Bipolar Lobes</h3>
  <p>Rather than jets forming that shoot straight out either end of the star, as was previously thought to occur, something completely different happens. But a picture is worth a thousand words, and these videos are made of thousands of pictures! Notice how magnetized material gets spewed out along the rotation axis, and then crumples up and fills two slowly-expanding lobes.</p>
<center>
<iframe width="420" height="315" src="//www.youtube.com/embed/3sZmvbtIi1I" frameborder="0" allowfullscreen></iframe>
<iframe width="420" height="315" src="//www.youtube.com/embed/sQQnIHFaVx0" frameborder="0" allowfullscreen></iframe>
</center>
<p>The first movie is a volume rendering of the <b>entropy</b>, and has the same colors as the slice movie above. The second is a voume rendering of the plasma <b>"beta"</b> parameter, which is the gas pressure divided by the magnetic pressure. In the yellow regions, the magnetic pressure is 10x the gas pressure, and in the blue regions the gas pressure is 10x the magnetic pressure. Prior to our simulation, this crumpled-up-jet structure had never been observed in supernova simulations. At the end of the simulation, the lobes were still slowly expanding, but not like an explosion. <b>The ultimate fate of the star is still unknown</b>. It is possible that the magnetic effects and neutrinos could eventually lead to an explosion, but it is also possible that the lobes will eventually fall back in.</p>

<br/>
  <h3>Speculation</h3>
  <p>Though our simulation is far from actually exploding, we noticed a few very interesting things that happen along the way.
  <ul>
    <li><b>Asymmetry</b> - Though we do not see a prompt jet-driven explosion, we do see a large amount of asymmetry (two big lobes rather than an expanding sphere). Seeing this much asymmetry makes us hopeful that our model is still consistent with observed remnants (e.g. W59B) thought to be from type Ic-bl supernovae.</li>
    <li><b>Black Hole Formation</b> - Though the lobes are expanding along the spin axis, matter is still rapidly falling in through the equator. As much mass as is in our entire sun is falling in each second, and it doesn't seem to be slowing down. If this continues, it is likely that a black hole will form. This would set the stage for a long gamma-ray burst and a Ic-bl supernova. Though we were not able to run this simulation long enough to see this happen, it will be a goal for simulations in the near future.</li>
    <li><b>Heavy Element Formation</b> - The magnetized flux tubes (the yellow parts in the movie) come from very near the neutron star, and so are very hot and neutron-rich (meaning there are many more neutrons than protons). This is a perfect environment to form very heavy elements (like gold and platinum) that regular core-collapse supernovae would not be able to synthesize.</li>
  </ul></p>


</div>
</div>
</div>
</div>
</body>
</html>
